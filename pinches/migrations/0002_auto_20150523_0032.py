# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pinches', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pinch',
            name='link',
            field=models.URLField(unique=True, max_length=256),
        ),
    ]
