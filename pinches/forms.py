from django.forms import ModelForm, ValidationError
from pinches.models import Pinch
from pinches.utils import thread_it, yt_dl, store_it
from django.core.exceptions import PermissionDenied, ImproperlyConfigured


class PinchModelForm(ModelForm):
    """
    Simple form for a Pinch
    """

    @thread_it
    def pinch_it(self, artist, song, link):
        exit_download = yt_dl(song, link)
        if exit_download == 0:
            exit_store = store_it(artist, song)
            if exit_store == 0:
                pinch = Pinch.objects.get(link=link)
                pinch.is_done = True
                pinch.save()
            else:
                raise PermissionDenied()
        else:
            raise ImproperlyConfigured()

    class Meta:
        model = Pinch
        fields = ['artist', 'song', 'link']
        labels = {
            'artist': ('Artist'),
            'song': ('Song'),
            'link': ('Youtube link')
        }
        help_text = {
            'artist': ('Ej.: Todo o Nada'),
            'song': ('Ej.: Sin mirar'),
            'link': ('Ej.: https://www.youtube.com/watch?v=dnoqifJ3UV0')
        }

    def clean(self):
        if Pinch.objects.filter(is_done=False).count() > 9:
            raise ValidationError("Demasiadas descargas juntas! Esperemos un poco!")
