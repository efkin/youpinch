from django.shortcuts import render
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from pinches.forms import PinchModelForm
from pinches.models import Pinch
from pinches.utils import escape


def index(request):
    return render(request, 'pinches/index.html')


def thanks(request):
    return render(request, 'pinches/thanks.html')


class PinchListView(ListView):

    are_done = True

    context_object_name = 'pinches'
    template_name = 'pinches/list.html'

    def get_queryset(self):
        return Pinch.objects.filter(is_done=self.are_done)


class NewPinchView(CreateView):
    template_name = 'pinches/new.html'
    form_class = PinchModelForm
    success_url = '/thanks/'
    model = Pinch

    def form_valid(self, form):
        form.pinch_it(escape(form.cleaned_data['artist']),
                      escape(form.cleaned_data['song']),
                      escape(form.cleaned_data['link']))

        return super(NewPinchView, self).form_valid(form)
