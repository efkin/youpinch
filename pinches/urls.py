from django.conf.urls import url
from pinches.views import NewPinchView, PinchListView
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^new/$', NewPinchView.as_view(), name='new_pinch'),
    url(r'^thanks/$', views.thanks, name='thanks'),
    url(r'^done/$', PinchListView.as_view(are_done=True), name='done_list'),
    url(r'^pending/$', PinchListView.as_view(are_done=False), name='pending_list'),

]
