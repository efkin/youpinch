from django.contrib import admin
from pinches.models import Pinch


class PinchAdmin(admin.ModelAdmin):
    list_display = ("link", "artist", "song", "is_done")
    list_filter = ['is_done']

admin.site.register(Pinch, PinchAdmin)
