from django.db import models


class Pinch(models.Model):
    """
    Simple class for pinches
    """
    artist = models.CharField(max_length=256)
    song = models.CharField(max_length=256)
    link = models.URLField(max_length=256, unique=True)
    is_done = models.BooleanField(default=False)
