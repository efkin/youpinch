from youpinch import settings
from threading import Thread
import subprocess


def thread_it(function):
    def decorator(*args, **kwargs):
        t = Thread(target=function, args=args, kwargs=kwargs)
        t.daemon = True
        t.start()
    return decorator


def yt_dl(song, link):
    command = 'youtube-dl -o "%s.mp3" -x --audio-format mp3 %s' % (song, link)
    code = subprocess.call(command, shell=True)
    return code


def store_it(artist, song):
    abs_path = "%s%s" % (settings.FTP_PATH, artist)
    command = 'if [ -d "%s" ]; \
              then mv "%s.mp3" "%s"; \
              else mkdir "%s"; mv "%s.mp3" "%s"; \
              fi' % (abs_path, song, abs_path, abs_path, song, abs_path)
    code = subprocess.call(command, shell=True)
    return code


def escape(s):
    return s.replace('`', '')
