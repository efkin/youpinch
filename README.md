YouPinch!
=========


Description
-----------
*YouPinch!*  is a YT audio extractor frontend for your local network.
It is written in Django 1.8 and python2.7.

Motivation
----------
Living in the countryside it's beautiful, but it will be nice to have better bandwith ;-).
At the moment we live with 6Mbs that turns into 4Mbs in practice because of the long radio-link.
We are around 20 people and have around 60 terminals in total connected to internet.
If we all listen to YT for a song it's not a sustainable situation.
So why not having a downloader connected to a FTP/NFS server? 

How it works
------------
* It retrieves asynchronously the media through ```youtube-dl```
* Creates a folder in the ftp music directory if it does not already exists
* And save the song.

Dependencies
------------
* **python 2.7**

* **youtube-dl**
    ```
    sudo curl https://yt-dl.org/latest/youtube-dl -o /usr/local/bin/youtube-dl
    sudo chmod a+x /usr/local/bin/youtube-dl
    ```
* **pip**
    ```
    sudo apt-get install pyhton-pip
    ```

How to deploy it (in a dirty way)
---------------------------------

* Create a user for youpinch
    ```
    sudo adduser youpinch
    sudo su youpinch
    cd
    ```

* Clone the repository
    ```
    git clone https://gitlab.com/efkin/youpinch.git
    ```

* Prepare the production enviroment (in a dirty way)
    
    - Install requirements
    ```
    cd youpinch
    sudo pip install --upgrade -r requirements.txt #if u run it from virtualenv you don't need sudo
    ```
    
    - Migrate db
    ```
    python manage.py migrate
    ```
    
    - Create superuser for admin interface
    ```
    python manage.py createsuperuser
    ```

    - Create dir for static files
    ```
    mkdir ../static
    ```

    - In youpinch/settings.py:
    ```
    DEBUG = False # It was True for debugging
    ...
    SECRET_KEY = "something random that you like and it looks like random"
    ...
    STATIC_URL = "/static/"
    STATIC_ROOT = "/home/youpinch/static"
    ...
    FTP_PATH = "/absolute/path/to/your/ftp/music/" # Important slash at the end!!!
    ```

    - Collect static files
    ```
    python manage.py collectstatic
    ```

* Grant permissions to youpinch user in ftp music path
    ```
    chown -R :youpinch /srv/ftp/music # or wherever it is
    chmod -R g+rw /srv/ftp/music # same as above
    ```

* Run the server!!!
    ```
    nohup /home/youpinch/youpinch/manage.py runserver
    ```


